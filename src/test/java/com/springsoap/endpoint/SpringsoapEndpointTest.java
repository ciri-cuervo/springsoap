package com.springsoap.endpoint;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;
import static org.springframework.ws.test.server.ResponseMatchers.validPayload;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import com.springsoap.config.WebServiceConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebServiceConfig.class })
public class SpringsoapEndpointTest {

	@Autowired
	private ApplicationContext applicationContext;

	private MockWebServiceClient mockClient;

	@Before
	public void createClient() {
		mockClient = MockWebServiceClient.createClient(applicationContext);
	}

	@Test
	public void testHeartbeat() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/HeartbeatRQ.xml"));
		byte[] response = Files.readAllBytes(Paths.get("src/test/resources/webservice/HeartbeatRS.xml"));
		Source requestPayload = new StringSource(new String(request, "utf8"));
		Source responsePayload = new StringSource(new String(response, "utf8"));
		Resource schemaResource = new ClassPathResource("schema/springsoap.xsd");

		mockClient.sendRequest(withPayload(requestPayload))
			.andExpect(payload(responsePayload))
			.andExpect(validPayload(schemaResource));
	}

	@Test
	public void testTransaction() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionRQ.xml"));
		byte[] response = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionRS.xml"));
		Source requestPayload = new StringSource(new String(request, "utf8"));
		Source responsePayload = new StringSource(new String(response, "utf8"));
		Resource schemaResource = new ClassPathResource("schema/springsoap.xsd");

		mockClient.sendRequest(withPayload(requestPayload))
			.andExpect(payload(responsePayload))
			.andExpect(validPayload(schemaResource));
	}

	@Test
	public void testAlarm() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/AlarmRQ.xml"));
		byte[] response = Files.readAllBytes(Paths.get("src/test/resources/webservice/AlarmRS.xml"));
		Source requestPayload = new StringSource(new String(request, "utf8"));
		Source responsePayload = new StringSource(new String(response, "utf8"));
		Resource schemaResource = new ClassPathResource("schema/springsoap.xsd");

		mockClient.sendRequest(withPayload(requestPayload))
			.andExpect(payload(responsePayload))
			.andExpect(validPayload(schemaResource));
	}

	@Test
	public void testTransactionStart() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionStartRQ.xml"));
		byte[] response = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionStartRS.xml"));
		Source requestPayload = new StringSource(new String(request, "utf8"));
		Source responsePayload = new StringSource(new String(response, "utf8"));
		Resource schemaResource = new ClassPathResource("schema/springsoap.xsd");

		mockClient.sendRequest(withPayload(requestPayload))
			.andExpect(payload(responsePayload))
			.andExpect(validPayload(schemaResource));
	}

	@Test
	public void testTransactionStop() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionStopRQ.xml"));
		byte[] response = Files.readAllBytes(Paths.get("src/test/resources/webservice/TransactionStopRS.xml"));
		Source requestPayload = new StringSource(new String(request, "utf8"));
		Source responsePayload = new StringSource(new String(response, "utf8"));
		Resource schemaResource = new ClassPathResource("schema/springsoap.xsd");

		mockClient.sendRequest(withPayload(requestPayload))
			.andExpect(payload(responsePayload))
			.andExpect(validPayload(schemaResource));
	}

}
