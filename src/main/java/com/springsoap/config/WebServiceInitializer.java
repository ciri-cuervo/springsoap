package com.springsoap.config;

import org.springframework.ws.transport.http.support.AbstractAnnotationConfigMessageDispatcherServletInitializer;

public class WebServiceInitializer extends AbstractAnnotationConfigMessageDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { WebServiceConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/ws/*" };
	}

	@Override
	public boolean isTransformWsdlLocations() {
		return true;
	}

}
