package com.springsoap.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.interceptor.PayloadLoggingInterceptor;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@Configuration
@EnableWs
@ComponentScan("com.springsoap.endpoint")
public class WebServiceConfig extends WsConfigurerAdapter {

	private static final String NAMESPACE_URI = "http://springsoap";

	@Bean
	public DefaultWsdl11Definition springsoap(XsdSchema springsoapSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("SpringsoapPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setSchema(springsoapSchema);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI);
		wsdl11Definition.setRequestSuffix("RQ");
		wsdl11Definition.setResponseSuffix("RS");
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema springsoapSchema() {
		return new SimpleXsdSchema(new ClassPathResource("schema/springsoap.xsd"));
	}

	@Override
	public void addInterceptors(List<EndpointInterceptor> interceptors) {
		// add logging interceptor
		interceptors.add(new PayloadLoggingInterceptor());

		// add validating interceptor
		PayloadValidatingInterceptor validatingInterceptor = new PayloadValidatingInterceptor();
		validatingInterceptor.setXsdSchema(springsoapSchema());
		validatingInterceptor.setValidateResponse(true);
		interceptors.add(validatingInterceptor);
	}

}
