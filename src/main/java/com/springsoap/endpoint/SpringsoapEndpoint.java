package com.springsoap.endpoint;

import java.lang.management.ManagementFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.springsoap.domain.AlarmRQ;
import com.springsoap.domain.HeartbeatRS;
import com.springsoap.domain.ObjectFactory;
import com.springsoap.domain.TransactionCompletRQ;
import com.springsoap.domain.TransactionStartRQ;
import com.springsoap.domain.TransactionType;

@Endpoint
public class SpringsoapEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringsoapEndpoint.class);
	private static final String NAMESPACE_URI = "http://springsoap";
	private static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "HeartbeatRQ")
	@ResponsePayload
	public HeartbeatRS heartbeat() throws DatatypeConfigurationException {
		long jvmUpTime = ManagementFactory.getRuntimeMXBean().getUptime();
		@SuppressWarnings("unused")
		Duration duration = DatatypeFactory.newInstance().newDuration(jvmUpTime);

		XMLGregorianCalendar systemUptime = DatatypeFactory.newInstance().newXMLGregorianCalendar("0001-01-01T00:00:00");

		HeartbeatRS response = new HeartbeatRS();
		response.setSystemUptime(systemUptime);
		response.setServiceRunning(false);
		response.setOpenTransactions(0);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "TransactionRQ")
	@ResponsePayload
	public JAXBElement<Boolean> registerTransaction(@RequestPayload JAXBElement<TransactionType> request) {
		
		return OBJECT_FACTORY.createTransactionRS(true);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AlarmRQ")
	@ResponsePayload
	public JAXBElement<Boolean> alarm(@RequestPayload AlarmRQ request) {
		
		return OBJECT_FACTORY.createAlarmRS(true);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "TransactionStartRQ")
	@ResponsePayload
	public JAXBElement<Boolean> transactionStart(@RequestPayload TransactionStartRQ request) {
		
		return OBJECT_FACTORY.createTransactionStartRS(true);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "TransactionCompletRQ")
	@ResponsePayload
	public JAXBElement<Boolean> transactionStop(@RequestPayload TransactionCompletRQ request) {
		
		return OBJECT_FACTORY.createTransactionCompletRS(true);
	}

}
